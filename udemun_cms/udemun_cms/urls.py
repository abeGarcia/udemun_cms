from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.utils.translation import activate
from django.contrib.auth import views
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    #url(r'^$', 'udemun_cms.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^markdown/', include( 'django_markdown.urls')),
)

urlpatterns += i18n_patterns('',
   url(r'^$', 'main.views.home', name='home'),
   url(_(r'^main/$'), 'main.views.home', name='home'),
   url(r'^markdown/', include( 'django_markdown.urls')),
   url(_(r'^main/'), include('main.urls')),
   url(_(r'^user/'), include('committees.urls')),
   url(_(r'^login/$'), 'django.contrib.auth.views.login', {'template_name':'login.html'},  name='login'),
   url(_(r'^logout/$'), 'django.contrib.auth.views.logout', {'next_page':'home'},  name='logout'),
   url(_(r'^password_reset/$'), 'django.contrib.auth.views.password_reset', {'post_reset_redirect' : 'password_reset_done'},  name='password_reset'),
   url(_(r'^password_reset/done/$'), 'django.contrib.auth.views.password_reset_done', name="password_reset_done"),
   url(_(r'^reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$'), 'django.contrib.auth.views.password_reset_confirm', {'post_reset_redirect' : 'reset/done/'}),
   url(_(r'^reset/done/$'), 'django.contrib.auth.views.password_reset_complete'),
)

