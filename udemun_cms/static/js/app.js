// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();
$(document).ready(function(e){

    if($("#clock").length != 0){
    
        // set the date we're counting down to
        var target_date = new Date("Feb 5, 2015").getTime();
         
        // variables for time units
        var days, hours, minutes, seconds;
         
        // get tag element
        var countdown = document.getElementById("clock");
         
        // update the tag with id "countdown" every 1 second
        setInterval(function () {
         
            // find the amount of "seconds" between now and target
            var current_date = new Date().getTime();
            var seconds_left = (target_date - current_date) / 1000;
         
            // do some time calculations
            days = parseInt(seconds_left / 86400);
            seconds_left = seconds_left % 86400;
             
            hours = parseInt(seconds_left / 3600);
            seconds_left = seconds_left % 3600;
             
            minutes = parseInt(seconds_left / 60);
            seconds = parseInt(seconds_left % 60);
    	
    	var days_title = " days ";
    	if(window.location.href.indexOf("es/") > -1) {
    	    days_title = " d&iacuteas ";
    	}
            // format countdown string + set tag value
    	if (days >= 1) { countdown.innerHTML = days + "<span>"+days_title+"</span>" }
    	else{
    	  countdown.innerHTML = mins(hours) + "<span>:</span>"
            + mins(minutes) + "<span>:</span>" + mins(seconds);
    	}

         
        }, 1000);
        
        function mins(min){
          var aux = String(min);
          if (min < 10) {
    	aux = "0"+aux;
          }
          return aux;
        }
    }
    
    /* maps */
    function initialize() {
        var mapOptions = {
          center: new google.maps.LatLng(25.65750, -100.42100),
          zoom: 15,
	  scrollwheel: false
        };
        var map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);
	
	var image = 'http://www.udemun.com/img/mark.png';
	var myLatLng = new google.maps.LatLng(25.65750, -100.42100);
	var marker = new google.maps.Marker({
	    position: myLatLng,
	    map: map,
	    icon: image,
	    title: 'UDEMUN 2015'
	});
    }
      
    google.maps.event.addDomListener(window, 'load', initialize);

    /* waypoints */
    var $head = $( '#bg-main' );
    $( '.article-waypoint' ).each( function(i) {
        var $el = $( this ),
            animClassDown = $el.data( 'animateDown' ),
            animClassUp = $el.data( 'animateUp' );

        $el.waypoint( function( direction ) {
            if( direction === 'down' && animClassDown ) {
                $head.attr('class', 'article article-header ' + animClassDown);
            }
            else if( direction === 'up' && animClassUp ){
                $head.attr('class', 'article article-header ' + animClassUp);
            }
        }, { offset: '100%' } );
    } );

    /* menu */
    if($("#menu-mob").length != 0){
        $('#menu-mob').on('click', function(e){
            e.preventDefault();
            // Handle Click
            $("#nav-primary").toggleClass("toggle");
            $('#menu li').removeClass("toggle");

        });

        $('#menu > li').on('click', function(e){
            e.stopPropagation();
            // Handle Click
            $('#menu > li').not(this).removeClass("toggle");
            $(this).toggleClass("toggle");
        });

        $('#menu .submenu > li').on('click', function(e){
            e.stopPropagation();
            // Handle Click
            $(this).toggleClass("toggle");
        });
    }

});