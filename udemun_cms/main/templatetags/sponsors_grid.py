from django import template
from main.models import Sponsors

register = template.Library()

@register.inclusion_tag('sponsors_grid.html')

def show_sponsors():
	try:
		sponsors = Sponsors.objects.all()
	except Sponsors.DoesNotExist:
		sponsors = None
		
	return { 'sponsors':sponsors }