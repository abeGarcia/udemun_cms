from django import template
from main.models import Footer, Category

register = template.Library()

@register.inclusion_tag('footer_location.html')

def show_location():
	info = Footer.objects.latest('created_at')
	categories = Category.objects.order_by("order").filter(parent=None)
	return { 'location' : info, 'categories':categories }