from django import template
from django.shortcuts import get_object_or_404
from main.models import Category, Article
from committees.models import Teacher, Student

register = template.Library()

@register.inclusion_tag('menu.html')

def show_menu(user):
	if user:
		try:
			teacher = Teacher.objects.get(user=user)
			students = Student.objects.filter(teacher=teacher).count()
		except Teacher.DoesNotExist:
			teacher = None
			students = None
	else:
		teacher = None
		students = None
	categories = Category.objects.order_by("order").filter(parent=None)
	total_categories = categories.count()
	menues = {}
	for value in categories:
		childs = Category.objects.exclude(parent=None).filter(parent=value)
		menues[value.order] = {
			'parent' : value,
			'child' : childs,
			'count' : Article.objects.filter(category=value).count()
		}
	article = Article.objects.all()
	return { 'categories' : categories, 'article' : article, 'menu':menues, 'teacher':teacher, 'user':user, 'students':students}

	