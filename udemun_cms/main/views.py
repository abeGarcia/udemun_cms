from .models import Home, Article, Person, Category
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.contrib import messages

# Create your views here.
def home(request):
	home = Home.objects.latest('created_at')
	president = Person.objects.filter(role__icontains='Secretar').filter(order=1).last()
	#messages.add_message(request, messages.INFO, 'Hello world.')
	return render(request, "main/home.html", {'article':home, 'president':president})

def footer(request):
	return render(request, "../templates/footer.html")

def default_article(request, slug, category, parent=1):
	_category = Category.objects.get(slug__iexact=category)
	article = get_object_or_404(Article, slug=slug, category=_category)

	if article.secretariat_enable:
		people = Person.objects.filter(job_type=1).order_by('order')
		max_number = people.last()
		values = {}
		for value in range(1, max_number.order+1):
			values[value] = {
				'team': Person.objects.filter(job_type=1).filter(order=value),
				'max': 12/Person.objects.filter(job_type=1).filter(order=value).count()
			}
		content = {'article':article, 'category':_category, 'people':values }
	else:
		content = {'article':article, 'category':_category}

	return render(request, "main/default_article.html", content)

def default_category(request, category):
	category = Category.objects.get(slug__iexact=category)
	childs = Category.objects.filter(parent=category)
	if childs.count() > 0:
		content = { 'article':childs, 'child':True }
	else:
		article = Article.objects.filter(category=category)
		content = { 'article':article, 'child':False }

	return render(request, "main/default_category.html", content )

