from django.conf.urls import patterns, include, url
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.utils.translation import activate


urlpatterns = patterns('main.views',
    url(r'^(?P<category>[-\w]+)/(?P<slug>[-\w]+)/$', 'default_article', name='main_default_article'),
    url(r'^(?P<parent>[-\w]+)/(?P<category>[-\w]+)/(?P<slug>[-\w]+)/$', 'default_article', name='main_default_article'),
    url(r'^(?P<category>[-\w]+)/$', 'default_category', name='main_default_category'),
)

