from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib import admin
from django_markdown.widgets import AdminMarkdownWidget
from django.utils.encoding import python_2_unicode_compatible

# Create your models here.
@python_2_unicode_compatible
class Category(models.Model):
	label = models.CharField(blank=True, max_length=50)
	slug = models.SlugField()
	parent = models.ForeignKey('self', blank=True, null=True,)
	topBanner = models.ImageField(upload_to='udemun_img')
   	topBannerTitle = models.TextField(max_length=100)
   	order = models.IntegerField(blank=True, null=True)

	class Meta:
		verbose_name_plural = "categories"

	def __str__(self):
		return self.label

class CategoryAdmin(admin.ModelAdmin):
	list_display = ('label', 'parent')
	prepopulated_fields = { 'slug' : ('label', )}
	formfield_overrides = {models.TextField: {'widget': AdminMarkdownWidget}}

class Person(models.Model):
	JOB_TYPE = (
        ('1', 'Secretariat'),
        ('2', 'Teacher'),
        ('3', 'Staff'),
        ('4', 'Other')
    )
    
	job_type = models.CharField(max_length=1, choices=JOB_TYPE, default='1')
	name = models.CharField(max_length=150)
	role = models.CharField(max_length=150)
	picture = models.ImageField(upload_to='udemun_img')
	contact = models.EmailField(max_length=150)
	order = models.IntegerField(blank=True)
	created_at = models.DateField(auto_now_add=True, blank=True)

	class Meta:
		verbose_name_plural = "people"

	def __str__(self):
		return self.name

class PersonAdmin(admin.ModelAdmin):
	list_display = ('name', 'role', 'contact', 'order')

class Article(models.Model):
	title = models.CharField(max_length=100)
	slug = models.SlugField()
	category = models.ForeignKey(Category)
	description = models.TextField()
	topBanner = models.ImageField(upload_to='udemun_img', null=True, blank=True)
   	topBannerTitle = models.TextField(max_length=100, null=True, blank=True)
	bottomBanner = models.ImageField(upload_to='udemun_img', null=True, blank=True)
   	bottomBannerTitle = models.TextField(max_length=100, null=True, blank=True)
	created_at = models.DateField(auto_now_add=True, blank=True)
	documentTitle = models.CharField(max_length=100, blank=True)
	document = models.FileField(upload_to='udemun_docs', null=True, blank=True)
	secretariat_enable = models.BooleanField()
	moderator = models.ForeignKey(Person, related_name="moderator", null=True, blank=True)
	pres = models.ForeignKey(Person, related_name="president", null=True, blank=True)
	order = models.IntegerField(blank=True, null=True)

	class Meta:
		#abstract = True
		ordering = ['created_at']
		verbose_name_plural = "articles"

	def __str__(self):
		return self.title

	def get_absoulte_url(self):
		return reverse('main_default_article', args=[self.category, self.slug])

class ArticleAdmin(admin.ModelAdmin):
	list_display = ('title', 'category', 'created_at')
	prepopulated_fields = { 'slug' : ('title', )}
	formfield_overrides = {models.TextField: {'widget': AdminMarkdownWidget}}

# class ArticleCommittee(Article):
# 	documentTitle = models.CharField(max_length=100, blank=True)
# 	document = models.FileField(upload_to='udemun_docs', null=True, blank=True)
# 	moderator = models.ForeignKey(Person, related_name="moderator", null=True, blank=True)
# 	pres = models.ForeignKey(Person, related_name="president", null=True, blank=True)

# 	class Meta:
# 		verbose_name_plural = "articles committee"

# class ArticleCommitteeAdmin(admin.ModelAdmin):
# 	list_display = ('title', 'category', 'created_at')
# 	prepopulated_fields = { 'slug' : ('title', )}
# 	formfield_overrides = {models.TextField: {'widget': AdminMarkdownWidget}}

class Footer(models.Model):
	location = models.TextField(max_length=200)
	schoolLogo = models.ImageField(upload_to='udemun_img')
	eventLogo = models.ImageField(upload_to='udemun_img')
	created_at = models.DateField(auto_now_add=True, blank=True)

	def __str__(self):
		return 'Footer-'+str(self.created_at)

class FooterAdmin(admin.ModelAdmin):
	formfield_overrides = {models.TextField: {'widget': AdminMarkdownWidget}}

class Sponsors(models.Model):
	name = models.TextField(max_length=200)
	sponsorLogo = models.ImageField(upload_to='udemun_img')
	created_at = models.DateField(auto_now_add=True, blank=True)

	def __str__(self):
		return self.name

class SponsorsAdmin(admin.ModelAdmin):
	formfield_overrides = {models.TextField: {'widget': AdminMarkdownWidget}}

class Home(models.Model):
	title = models.CharField(max_length=100)
	slug = models.SlugField()
	description = models.TextField()
	bottomBanner = models.ImageField(upload_to='udemun_img', null=True, blank=True, max_length=300)
	bottomBannerTitle = models.TextField(max_length=100, null=True, blank=True)
	topBannerTitle = models.TextField(max_length=100, null=True, blank=True)
	created_at = models.DateField(auto_now_add=True, blank=True)

	class Meta:
		ordering = ['created_at']
		verbose_name_plural = "home"

	def __str__(self):
		return 'Home-'+str(self.created_at)

class HomeAdmin(admin.ModelAdmin):
	formfield_overrides = {models.TextField: {'widget': AdminMarkdownWidget}}
