from django.contrib import admin
from .models import Category, CategoryAdmin, Person, PersonAdmin, Home, HomeAdmin, Article, ArticleAdmin, Footer, FooterAdmin, Sponsors, SponsorsAdmin
#from .models import ArticleCommittee, ArticleCommitteeAdmin
from modeltranslation.admin import TranslationAdmin

class TransCategoryAdmin(CategoryAdmin,TranslationAdmin):
    class Media:
        js = (
            'modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js',
            'modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
        }

class TransPersonAdmin(PersonAdmin, TranslationAdmin):
    class Media:
        js = (
            'modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js',
            'modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
        }

class TransHomeAdmin(HomeAdmin,TranslationAdmin):
    class Media:
        js = (
            'modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js',
            'modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
        }

class TransArticleAdmin(ArticleAdmin, TranslationAdmin):
    class Media:
        js = (
            'modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js',
            'modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
        }

class TransFooterAdmin(FooterAdmin, TranslationAdmin):
    class Media:
        js = (
            'modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js',
            'modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
        }

class TransSponsorsAdmin(SponsorsAdmin, TranslationAdmin):
    class Media:
        js = (
            'modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js',
            'modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
        }

# class TransArticleCommitteeAdmin(ArticleCommitteeAdmin, TranslationAdmin):
#     class Media:
#         js = (
#             'modeltranslation/js/force_jquery.js',
#             'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js',
#             'modeltranslation/js/tabbed_translation_fields.js',
#         )
#         css = {
#             'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
#         }

# Register your models here.
admin.site.register(Category, TransCategoryAdmin)
admin.site.register(Person, TransPersonAdmin);
admin.site.register(Home, TransHomeAdmin);
admin.site.register(Article, TransArticleAdmin);
admin.site.register(Footer, TransFooterAdmin);
admin.site.register(Sponsors, TransSponsorsAdmin);
#admin.site.register(ArticleCommittee, TransArticleCommitteeAdmin);
