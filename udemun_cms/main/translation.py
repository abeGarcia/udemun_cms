from modeltranslation.translator import translator, TranslationOptions
from .models import Category, Person, Home, Article, Footer, Sponsors
#from .models import ArticleCommittee

class CategoryTranslationOptions(TranslationOptions):
    fields = ('label', 'slug', 'topBannerTitle')
    empty_values = ''
    fallback_values = 'None'
    fallback_undefined = 'None'

class PersonTranslationOptions(TranslationOptions):
    fields = ('role',)
    empty_values = ''
    fallback_values = 'None'
    fallback_undefined = 'None'

class HomeTranslationOptions(TranslationOptions):
    fields = ('title', 'slug', 'description', 'bottomBannerTitle', 'topBannerTitle')
    empty_values = ''
    fallback_values = 'None'
    fallback_undefined = 'None'

class ArticleTranslationOptions(TranslationOptions):
    fields = ('title', 'slug', 'description', 'topBannerTitle', 'bottomBannerTitle', 'documentTitle',)
    empty_values = ''
    fallback_values = 'None'
    fallback_undefined = 'None'

class FooterTranslationOptions(TranslationOptions):
    fields = ('location',)
    empty_values = ''
    fallback_values = 'None'
    fallback_undefined = 'None'

class SponsorsTranslationOptions(TranslationOptions):
    fields = ('name',)
    empty_values = ''
    fallback_values = 'None'
    fallback_undefined = 'None'

# class ArticleCommitteeTranslationOptions(TranslationOptions):
#     fields = ('documentTitle',)

translator.register(Category, CategoryTranslationOptions)
translator.register(Person, PersonTranslationOptions)
translator.register(Home, HomeTranslationOptions)
translator.register(Article, ArticleTranslationOptions)
translator.register(Footer, FooterTranslationOptions)
translator.register(Sponsors, SponsorsTranslationOptions)
#translator.register(ArticleCommittee, ArticleCommitteeTranslationOptions)