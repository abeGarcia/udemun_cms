from django.conf.urls import patterns, include, url
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.utils.translation import activate

urlpatterns = patterns('committees.views',
    url(_(r'^registration/$'), 'new_teacher', name='teacher_register'),
    url(_(r'^profile/$'), 'teacher_profile', name='teacher_profile'),
    url(_(r'^profile/edit'), 'teacher_profile_edit', name='teacher_profile_edit'),
    url(_(r'^students/$'), 'teacher_students', name='teacher_students'),
    url(_(r'^student/(?P<pk>\d+)/$'), 'teacher_student_edit', name='teacher_student'),
    url(_(r'^students/add/$'), 'teacher_new_student', name='teacher_new_student'),
)
