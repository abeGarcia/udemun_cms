from modeltranslation.translator import translator, TranslationOptions
from .models import Country, Committee
#from .models import ArticleCommittee

class CountryTranslationOptions(TranslationOptions):
    fields = ('name',)
    empty_values = ''
    fallback_values = 'None'
    fallback_undefined = 'None'

class CommitteeTranslationOptions(TranslationOptions):
    fields = ('name',)
    empty_values = ''
    fallback_values = 'None'
    fallback_undefined = 'None'

translator.register(Country, CountryTranslationOptions)
translator.register(Committee, CommitteeTranslationOptions)
