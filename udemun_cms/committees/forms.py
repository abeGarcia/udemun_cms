from django.forms import ModelForm
from django import forms
from .models import Teacher, Student
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

LANGUAGE = (
    ('1', 'English'),
    ('2', 'Spanish')
)

class TeacherForm(ModelForm):
	username = forms.CharField(max_length=30, label= _('User name'))
	password = forms.CharField(max_length=20, widget=forms.PasswordInput(), label= _('Create a password'))
	confirm_password = forms.CharField(max_length=20, widget=forms.PasswordInput(), label= _('Reenter password'))

	class Meta:
		model = Teacher
		exclude = ['status', 'committees', 'user']
		fields = ('username', 'password', 'confirm_password', 'first_name', 'last_name', 'email', 'school', 'students_number', 'grade_type', 'language_preference')
		labels = {
			'first_name': _('First Name'),
			'last_name': _('Last Name'),
			'email': _('Email'),
			'school': _('School'),
			'students_number': _('Number of students'),
			'grade_type': _('Grade'),
			'language_preference': _('Language of preference'),
		}

class TeacherUpdateForm(ModelForm):
	class Meta:
		model = Teacher
		exclude = ['status', 'committees', 'user']
		fields = ('first_name', 'last_name', 'email', 'school', 'grade_type', 'students_number', 'language_preference')
		labels = {
			'first_name': _('First Name'),
			'last_name': _('Last Name'),
			'email': _('Email'),
			'school': _('School'),
			'students_number': _('Number of students'),
			'grade_type': _('Grade'),
			'language_preference': _('Language of preference'),
		}

class StudentForm(ModelForm):
	class Meta:
		model = Student
		exclude = ['teacher']
