from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, Group
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.template.loader import get_template
from django.template import Context
from django.utils.translation import ugettext_lazy as _

from .models import Teacher, Student, Committee
from .forms import TeacherForm, StudentForm, TeacherUpdateForm

# Create your views here.
def new_teacher(request):
    if request.method == 'POST':
        teacher = Teacher(status="P", language_preference=request.POST.get('language_preference', '1'))
        form = TeacherForm(data=request.POST, instance=teacher)
        postDict = request.POST.copy()
        if form.is_valid():
			#create user
			user = User.objects.create_user(postDict['username'], postDict['email'], postDict['password'])
			user.groups.add(Group.objects.get(name='Teachers'))
			user.last_name = postDict['last_name']
			user.first_name = postDict['first_name']
			user.save()

			form.save()
			
			aux = Teacher.objects.get(email=postDict['email'])
			aux.user = user
			aux.save()

			#send email
			emailHtml = get_template('committees/email_new_teacher_notification.html')
			info = Context({ 'teacher': teacher })
			html_content = emailHtml.render(info)
			#send_mail('Django test', 'Django test message.', 'django@udemun.com',['ab.garcia@outlook.com'], fail_silently=False)
			#udemun admin notification
			msg = EmailMessage(_('New teacher registration'), html_content, 'admin@udemun.com', ['info@udemun.com'])
			msg.content_subtype = "html"  # Main content is now text/html
			msg.send()
			#udemun teacher notification
			teacherEmail = postDict['email']
			emailHtml = get_template('committees/email_new_teacher_registration.html')
			info = Context({ 'teacher': teacher, 'user' : postDict['username'], 'password': postDict['password']  })
			html_content = emailHtml.render(info)
			msg = EmailMessage(_('Udemun registration'), html_content, 'admin@udemun.com', [teacherEmail])
			msg.content_subtype = "html"  # Main content is now text/html
			msg.send()
			return redirect('home')
    else:
        form = TeacherForm()
    return render(request, "committees/new_teacher.html", {'form': form})

@login_required(login_url='login')
def teacher_profile(request):
	teacher = Teacher.objects.get(user=request.user)
	return render(request, "committees/teacher_profile.html", { 'teacher':teacher })

@login_required(login_url='login')
def teacher_students(request):
	teacher = Teacher.objects.get(user=request.user)
	students = Student.objects.filter(teacher=teacher)
	return render(request, "committees/teacher_students.html", { 'students':students })

@login_required(login_url='login')
def teacher_profile_edit(request):
	teacher = Teacher.objects.get(user=request.user)

	form = TeacherUpdateForm(request.POST or None, instance=teacher)
	if request.method == 'POST':
		if form.is_valid():
			form.save()

			aux = Teacher.objects.get(user=request.user)
			aux.status = "P"
			aux.save()
			
			#send email for teacher update
			emailHtml = get_template('committees/email_teacher_update_notification.html')
			info = Context({ 'teacher': teacher })
			html_content = emailHtml.render(info)
			#udemun admin notification
			msg = EmailMessage(_('New teacher profile update'), html_content, 'admin@udemun.com', ['info@udemun.com'])
			msg.content_subtype = "html"  # Main content is now text/html
			msg.send()
			#udemun teacher notification
			teacherEmail = teacher.email
			emailHtml = get_template('committees/email_teacher_update.html')
			info = Context({ 'teacher': teacher, })
			html_content = emailHtml.render(info)
			msg = EmailMessage(_('Udemun Profile Update'), html_content, 'admin@udemun.com', [teacherEmail])
			msg.content_subtype = "html"  # Main content is now text/html
			msg.send()
			return redirect('teacher_profile')
	return render(request, "committees/teacher_profile_edit.html", { 'form':form })

@login_required(login_url='login')
def teacher_student_edit(request, pk='0'):
	student = get_object_or_404(Student, pk=pk)
	committee_queryset = Committee.objects.filter(school=student.teacher)
	committee_student = Student.objects.exclude(pk=pk).exclude(committee=None)
	for aux in committee_student:
		committee_queryset = committee_queryset.exclude(pk=aux.committee.pk)

	form = StudentForm(request.POST or None, instance=student)
	form.fields['committee'].queryset = committee_queryset
	if request.method == 'POST':
		if form.is_valid():
			form.save()

			#send student email
			studentEmail = student.email
			emailHtml = get_template('committees/email_student.html')
			info = Context({ 'student': student })
			html_content = emailHtml.render(info)
			msg = EmailMessage(_('Student Committee Assignment'), html_content, 'admin@udemun.com', [studentEmail])
			msg.content_subtype = "html"  # Main content is now text/html
			msg.send()
			return redirect('teacher_students')
	return render(request, "committees/teacher_student_edit.html", { 'form':form })

@login_required(login_url='login')
def teacher_new_student(request):
	teacher = Teacher.objects.get(user=request.user)
	students = Student.objects.filter(teacher=teacher).count()
	if request.method == 'POST':
		student = Student(teacher=teacher)
		form = StudentForm(data=request.POST, instance=student)
		postDict = request.POST.copy()
		if students < teacher.students_number:
			if form.is_valid():
				form.save()

				#send student email
				studentEmail = student.email
				emailHtml = get_template('committees/email_student.html')
				info = Context({ 'student': student })
				html_content = emailHtml.render(info)
				msg = EmailMessage(_('Student Committee Assignment'), html_content, 'admin@udemun.com', [studentEmail])
				msg.content_subtype = "html"  # Main content is now text/html
				msg.send()
				students = Student.objects.filter(teacher=teacher).count()
				if students == teacher.students_number:
					#send email when add total of students
					emailHtml = get_template('committees/email_teacher_students_complete_notification.html')
					info = Context({ 'teacher': teacher })
					html_content = emailHtml.render(info)
					#udemun admin notification
					msg = EmailMessage(_('Teacher group complete'), html_content, 'admin@udemun.com', ['info@udemun.com'])
					msg.content_subtype = "html"  # Main content is now text/html
					msg.send()

				return redirect('teacher_students')
		else:
			return redirect('teacher_students')
	else:
		committee_queryset = Committee.objects.filter(school=teacher)
		committee_student = Student.objects.exclude(committee=None)
		for aux in committee_student:
			committee_queryset = committee_queryset.exclude(pk=aux.committee.pk)

		student = Student(teacher=teacher)
		form = StudentForm(instance=student)
		form.fields['committee'].queryset = committee_queryset
	return render(request, "committees/teacher_new_student.html", {'form': form})