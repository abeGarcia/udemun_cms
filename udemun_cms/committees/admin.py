from django.contrib import admin
from django.contrib.admin.helpers import ActionForm
from django import forms
from .models import Country, CountryAdmin, Committee, Teacher, TeacherAdmin, Student, StudentAdmin
from django_markdown.widgets import AdminMarkdownWidget
from modeltranslation.admin import TranslationAdmin

#custom action for committees groups
class UpdateActionForm(ActionForm):
	currentTeachers = Teacher.objects.filter(status='A')
	customTeachers = []
	for item in currentTeachers:
		customTeachers.append([item.pk, item])
	teacher = forms.ChoiceField(choices=customTeachers, required=False)

def update_committee(modeladmin, request, queryset):
    school = request.POST['teacher']
    queryset.update(school=school, status='O')

update_committee.short_description = "Assign a teacher/school to committee"

def clear_committee(modeladmin, request, queryset):
    queryset.update(school='', status='A')

clear_committee.short_description = "Unassign a teacher/school to committee"

class CommitteeAdmin(admin.ModelAdmin):
	action_form = UpdateActionForm
	actions = [update_committee, clear_committee]
	list_display = ('name', 'country', 'language', 'range_type', 'status', "school_name")
	list_filter = ('country', 'status', 'range_type', "school", "name", "language")
	search_fields = ('name', 'country', "school_name")
	def save_model(self, request, obj, form, change):
		if change:
			if obj.school:
				obj.status = 'O'
		obj.save()

class TransCountryAdmin(CountryAdmin,TranslationAdmin):
    class Media:
        js = (
            'modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js',
            'modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
        }

class TransCommitteeAdmin(CommitteeAdmin,TranslationAdmin):
    class Media:
        js = (
            'modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js',
            'modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
        }
# Register your models here.
admin.site.register(Country, TransCountryAdmin)
admin.site.register(Committee, TransCommitteeAdmin);
admin.site.register(Teacher, TeacherAdmin);
admin.site.register(Student, StudentAdmin);
