from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib import admin
from django_markdown.widgets import AdminMarkdownWidget
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

# Create your models here.
LANGUAGE = (
	('', _('Select your language')),
    ('1', _('English')),
    ('2', _('Spanish')),
)

COMMITTEE_STATUS = (
	('A', 'Available'),
	('N', 'Not available'),
	('O', 'Occupied'),
)

RANGE = (
	('', _('Select your grade of preference')),
	('H', _('High School')),
	('M', _('Middle School')),
)

GRADE_YEAR = (
	('', _('Select a grade or year of your student')),
	('1', _('1 Grade / Year')),
	('2', _('2 Grade / Year')),
	('3', _('3 Grade / Year')),
)

TEACHER_STATUS = (
	('A', 'Accepted'),
	('P', 'Pending'),
	('R', 'Rejected'),
)

@python_2_unicode_compatible
class Country(models.Model):
	name = models.CharField(max_length=100)

	class Meta:
		verbose_name_plural = "countries"

	def __str__(self):
		return self.name

class CountryAdmin(admin.ModelAdmin):
	list_display = ('name',)

class Committee(models.Model):    
	country = models.ForeignKey(Country)
	language = models.CharField(max_length=1, choices=LANGUAGE, default='1')
	name = models.CharField(max_length=100)
	status = models.CharField(max_length=1, choices=COMMITTEE_STATUS, default='A')
	range_type = models.CharField(max_length=1, choices=RANGE, default='H')
	school = models.ForeignKey('Teacher', limit_choices_to={'status': 'A'}, blank=True, null=True)

	class Meta:
		verbose_name_plural = "committees"

	def __str__(self):
		return self.name+' / '+self.country.name

	def school_name(self):
		if self.school:
			return self.school.school
		else:
			return "-"

class Teacher(models.Model):
	user = models.OneToOneField('auth.User', related_name='profile' , blank=True, null=True)
	first_name = models.CharField(max_length=150)
	last_name = models.CharField(max_length=150)
	email = models.EmailField(max_length=150, unique=True)
	school = models.CharField(max_length=100, unique=True)
	students_number = models.IntegerField()
	language_preference = models.CharField(max_length=1, choices=LANGUAGE, default='')
	grade_type = models.CharField(max_length=1, choices=RANGE, default='')
	status = models.CharField(max_length=1, choices=TEACHER_STATUS, default='P')
	start_time = models.DateTimeField(auto_now_add=True)
	last_active = models.DateTimeField(auto_now=True)
	#addlanguage

	class Meta:
		verbose_name_plural = "teacher"

	def __str__(self):
		return self.school+" / "+self.first_name

	def teacher_name(self):
		return self.first_name+" "+self.last_name

class TeacherAdmin(admin.ModelAdmin):
	list_display = ('teacher_name', 'email', 'school', 'status', 'last_active', 'user')

class Student(models.Model):
	first_name = models.CharField(max_length=150)
	last_name = models.CharField(max_length=150)
	grade = models.CharField(max_length=1, choices=GRADE_YEAR, default='')
	email = models.EmailField(blank=True, max_length=150, unique=True)
	teacher = models.ForeignKey(Teacher, limit_choices_to={'status': 'A'})
	start_time = models.DateTimeField(auto_now_add=True)
	last_active = models.DateTimeField(auto_now=True)
	committee = models.ForeignKey(Committee, blank=True, null=True)

	class Meta:
		verbose_name_plural = "students"

	def __str__(self):
		return self.first_name+" "+self.last_name

	def student_name(self):
		return self.first_name+" "+self.last_name

	def school_name(self):
		return self.teacher.school

	def teacher_name(self):
		return self.teacher.teacher_name

	def country(self):
		if self.committee:
			return self.committee.country.name
		else:
			return "None"

	def committee_name(self):
		if self.committee:
			return self.committee.name
		else:
			return "None"

class StudentAdmin(admin.ModelAdmin):
	list_display = ('student_name', 'grade', 'committee_name', 'country', 'teacher', 'school_name')
	list_filter = ('grade', 'teacher__school', 'committee__country__name')
	search_fields = ('student_name', 'country', 'school_name')

# class UdemStudent(models.Model):
# 	user = models.OneToOneField('auth.User', related_name='profile' , blank=True, null=True)
# 	status = models.CharField(max_length=1, choices=TEACHER_STATUS, default='P')
# 	first_name = models.CharField(max_length=150)
# 	last_name = models.CharField(max_length=150)
# 	grade = models.IntegerField()
# 	email = models.EmailField(blank=True, max_length=150, unique=True)
# 	teacher = models.ForeignKey(Teacher, limit_choices_to={'status': 'A'})
# 	start_time = models.DateTimeField(auto_now_add=True)
# 	last_active = models.DateTimeField(auto_now=True)
# 	committee = models.ForeignKey(Committee, blank=True, null=True)

# 	class Meta:
# 		verbose_name_plural = "students"

# 	def __str__(self):
# 		return self.name_complete

# 	def student_name(self):
# 		return self.first_name+" "+self.last_name

# 	def school_name(self):
# 		return self.teacher.school

# 	def teacher_name(self):
# 		return self.teacher.name_complete

# 	def country(self):
# 		if self.committee:
# 			return self.committee.country.name
# 		else:
# 			return "None"

# 	def committee_name(self):
# 		if self.committee:
# 			return self.committee.name
# 		else:
# 			return "None"

# class UdemStudentAdmin(admin.ModelAdmin):
# 	list_display = ('student_name', 'grade', 'committee_name', 'country', 'teacher_name', 'school_name')
# 	list_filter = ('grade', 'teacher__school', 'committee__country__name')
# 	search_fields = ('student_name', 'country', 'school_name')