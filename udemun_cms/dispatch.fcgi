#!/home/udemunco/.env/bin/python2.7

import sys,os

sys.path.append("/home/udemunco/.env/lib/python2.7/site-packages")
sys.path.append("/home/udemunco/udemun/udemun_cms")
sys.path.append("/home/udemunco/udemun/udemun_cms/media")

os.chdir("/home/udemunco/udemun/udemun_cms")

os.environ['DJANGO_SETTINGS_MODULE'] = "udemun_cms.settings"

from django.core.servers.fastcgi import runfastcgi
runfastcgi(method="threaded", daemonize="false")
